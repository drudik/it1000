 import { useEffect, useState } from "react";
import { interval, Subject, fromEvent } from "rxjs";
import { takeUntil, debounceTime, buffer, map, filter } from "rxjs/operators";
  
function App() {
  
  const [sec, setSec] = useState(0);
  const [status, setStatus] = useState('');
 
  useEffect(() => {
    const timer$ = new Subject();
    
    interval(1000)
      .pipe(takeUntil(timer$))
      .subscribe(() => {
        if (status === "run") {
          setSec(val => val + 1000);
        }
    });
    
    return () => {
      timer$.next();
      timer$.complete();
      };
    }, [status]);
 
    const start = () => {
      setStatus("run");
    };
  
    const stop = () => {
      setStatus("stop");
      setSec(0);
    };
  
    const reset = () => {
      setSec(0);
    };
    
    
    const wait = () => {
      const click$ = fromEvent(document, 'click');
      const doubleClick$ = click$.pipe(
          buffer(click$.pipe(debounceTime(300))),
          map(clicks => clicks.length),
          filter(clicksLength => clicksLength >= 2)
      );
      doubleClick$.subscribe(() => setStatus('wait'));
      };
 
  return (
    <div>
      <span> {new Date(sec).toISOString().slice(11, 19)}</span>
      <button className="start-button" onClick={start}>
        Start
      </button>
      <button className="stop-button" onClick={stop}>
        Stop
      </button>
      <button onClick={reset}>Reset</button>
      <button onClick={wait}>Wait</button>
      {/* <button  >Wait</button> */}
    </div>
  );
}
 
export default App;